import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MainComponent } from "./main.component";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { MainRoutingModule } from "./main-routing.module";

/// CommonModule Module chức năng

@NgModule({
    declarations: [MainComponent],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        HttpClientModule,
        MainRoutingModule
    ],
    providers: [
        HttpClient
    ],
    bootstrap: []
})
export class MainModule { }
