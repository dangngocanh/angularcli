import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MainComponent } from "./main.component";

const routes: Routes = [
   {
       path: '',
       component: MainComponent
   }
];

@NgModule({

  // .forRoot() giống như là router TỔng
  // .forChild()  giống như đây là routing con của ứng dụng
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class MainRoutingModule {}
