import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { User } from '../models';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

    private currentUserSubject: BehaviorSubject<User>;

    public currentUser: Observable<User>;

    public getCurrentUser = localStorage.getItem('currentUser')
    
    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(String(this.getCurrentUser)));
        //asObservable Ngăn chặn rò rỉ phía quan sát viên khi mọi người có thể tiếp theo quan sát được
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post<any>(`${environment.apiUrl}/users/authenticate`, { username, password })
            .pipe(map(user => {
                // đăng nhập thành công nếu có mã thông báo jwt trong phản hồi
                if (user && user.token) {
                    // lưu trữ chi tiết người dùng và mã thông báo jwt trong bộ nhớ cục bộ để giữ cho người dùng luôn đăng nhập giữa các lần làm mới trang
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }

                return user;
            }));
    }

    logout() {
        // Xóa User hiện tại khỏi bộ nhớ 
        localStorage.removeItem('currentUser');
        // this.currentUserSubject.next(null);
        this.currentUserSubject.complete();
    }
}