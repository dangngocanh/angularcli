import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.scss"]
})
export class MainComponent implements OnInit {
  users: any = [
    {
      name: "Anh 1",
      age: 22
    },
    {
      name: "Anh 2",
      age: 23
    },
    {
      name: "Anh 3",
      age: 24
    }
  ];
  time = new Date();

  constructor() {}

  ngOnInit() {}
  
}
